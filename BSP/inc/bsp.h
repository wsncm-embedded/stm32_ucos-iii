#ifndef _BSP_H_
#define _BSP_H

#include "stm32f10x.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define  ENABLE_INT()      OS_CRITICAL_EXIT()     /* 使能全局中断 */
#define  DISABLE_INT()     OS_CRITICAL_ENTER()    /* 禁止全局中断 */

#define RCC_ALL_LED 	(RCC_APB2Periph_GPIOB)

#define GPIO_PORT_LED1  GPIOB
#define GPIO_PIN_LED1	GPIO_Pin_0

void bsp_init(void);
void bsp_tick_init (void);
void bsp_led_on(uint8_t _no);
void bsp_led_off(uint8_t _no);
void bsp_led_toggle(uint8_t _no);

#endif
