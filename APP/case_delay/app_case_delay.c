 /**
 * @brief       : 系统时间管理
 *                系统节拍: 测试任务周期执行，打印时间间隔。
 *                真实时间: 测试任务周期执行，打印时间间隔。
 *                取消延时: 中断中发送'1'来取消系统节拍延时任务，打印当前时间。
 * @file        : app_case_delay.c
 * @author      : cuihongpeng
 * @version     : v0.0.1
 * @date        : 2015/10/12
 *
 * Change Logs  : 
 *
 * Date        Version      Author      Notes
 * 2015/10/12  v0.0.1    cuihongpeng    first version
 */
#include <includes.h>

OS_SEM sem_isr;
OS_SEM sem_resume_delay;
OS_SEM sem_pend;

static OS_TCB  app_start_task_tcb;
static CPU_STK app_start_task_stk[APP_CFG_TASK_START_STK_SIZE];

static OS_TCB app_delay_by_sys_beat_task_tcb;
static CPU_STK app_delay_by_sys_beat_task_stk[APP_CFG_TASK_COMMON_STK_SIZE];

static OS_TCB app_delay_by_real_time_task_tcb;
static CPU_STK app_delay_by_real_time_task_stk[APP_CFG_TASK_COMMON_STK_SIZE];

static OS_TCB app_delay_resume_task_tcb;
static CPU_STK app_delay_resume_task_stk[APP_CFG_TASK_COMMON_STK_SIZE];

static void app_task_create(void);
static void app_event_create(void);
static void app_start_task(void *p_arg);

int main(void)
{
    OS_ERR  err;  
    
    /* 初始化uC/OS-III 内核 */
    OSInit(&err);  
    
    OSTaskCreate((OS_TCB       *)&app_start_task_tcb,               /* 任务控制块地址 */           
                 (CPU_CHAR     *)"App Task Start",                  /* 任务名*/
                 (OS_TASK_PTR   )app_start_task,                    /* 启动任务函数地址 */
                 (void         *)0,                                 /* 传递给任务的参数*/
                 (OS_PRIO       )APP_CFG_TASK_START_PRIO,           /* 任务优先级*/
                 (CPU_STK      *)&app_start_task_stk[0],            /* 堆栈基地址 */
                 (CPU_STK_SIZE  )APP_CFG_TASK_START_STK_SIZE / 10,  /* 堆栈监测区*/
                 (CPU_STK_SIZE  )APP_CFG_TASK_START_STK_SIZE,       /* 堆栈空间大小 */
                 (OS_MSG_QTY    )0,                                 /* 本任务支持接受的最大消息数 */
                 (OS_TICK       )0,                                 /* 设置时间*/
                 (void         *)0,                                 /* 堆栈空间大小 */  
                 (OS_OPT        )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                 (OS_ERR       *)&err);

    OSStart(&err);                                               
    
    (void)&err;
    
    return (0);
}

/**
 * @brief 启动任务，单次执行
 */
static void app_start_task(void *p_arg)
{
    OS_ERR      err;
    
    (void)p_arg;
    
    CPU_Init();
    bsp_init();
    bsp_tick_init();
    app_event_create();
    app_task_create();   
    
    while (1)
    {  
		printf("start task case!\r\n\r\n");
		OSTaskDel(&app_start_task_tcb, &err);
    }
}

/**
 * @brief 调用系统延时函数 OSTimeDly
 */
void app_delay_by_sys_beat_task(void *p_arg)
{
    while (1)                                            
    {
		static uint32_t run_count =  0;
        RUN_COUNTER(run_count);
		OS_ERR err;
		
        uint32_t previous_time = OSTimeGet(&err);
        OSTimeDly(DELAY_TIMER, OS_OPT_TIME_DLY, &err);
        
        uint32_t last_time = OSTimeGet(&err);
        uint32_t offset_time = last_time - previous_time;
        
        OSSchedLock(&err);
        printf("OSTimeDly task: %d\r\n", offset_time);
        OSSchedUnlock(&err);
    }
}

/**
 * @brief 调用系统延时函数 OSTimeDlyHMSM()
 */
void app_delay_by_real_time_task(void *p_arg)
{
    while (1)                                            
    {
		static uint32_t run_count =  0;
        RUN_COUNTER(run_count);
		OS_ERR err;
		
        uint32_t previous_time = OSTimeGet(&err);
        OSTimeDlyHMSM(0,0,1,0,OS_OPT_TIME_DLY,&err);
        
        uint32_t last_time = OSTimeGet(&err);
        uint32_t offset_time = last_time - previous_time;
        
        OSSchedLock(&err);
        printf("OSTimeDlyHMSM task: %d\r\n\r\n", offset_time);
        OSSchedUnlock(&err);

    }
}

/**
 * @brief 取消延时任务
 */
void app_delay_resume_task(void *p_arg)
{
    while (1)                                            
    {
        OS_ERR err;
		OSSemPend(&sem_isr, 0, OS_OPT_PEND_BLOCKING, NULL, &err);
        OSTimeDlyResume(&app_delay_by_sys_beat_task_tcb, &err);
    }
}

/**
 * @brief 创建信号量
 */
static void app_event_create(void)
{
    OS_ERR       err;
    OSSemCreate(&sem_resume_delay, "sem resume delay", 0, &err);
    OSSemCreate(&sem_isr, "sem_isr", 0, &err);
    OSSemCreate(&sem_pend, "sem_pend", 0, &err);
}

/**
 * @brief 创建任务
 */
static void app_task_create(void)
{
    OS_ERR      err;
    
    OSTaskCreate((OS_TCB       *)&app_delay_by_sys_beat_task_tcb,            
                 (CPU_CHAR     *)"app delay by sys beat task",
                 (OS_TASK_PTR   )app_delay_by_sys_beat_task, 
                 (void         *)0,
                 (OS_PRIO       )APP_CFG_TASK_COMMON_PRIO,
                 (CPU_STK      *)&app_delay_by_sys_beat_task_stk[0],
                 (CPU_STK_SIZE  )APP_CFG_TASK_COMMON_STK_SIZE / 10,
                 (CPU_STK_SIZE  )APP_CFG_TASK_COMMON_STK_SIZE,
                 (OS_MSG_QTY    )0,
                 (OS_TICK       )0,
                 (void         *)0,
                 (OS_OPT        )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                 (OS_ERR       *)&err);
				 
    OSTaskCreate((OS_TCB       *)&app_delay_by_real_time_task_tcb,            
                 (CPU_CHAR     *)"app delay by real time task",
                 (OS_TASK_PTR   )app_delay_by_real_time_task, 
                 (void         *)0,
                 (OS_PRIO       )APP_CFG_TASK_COMMON_PRIO + 1,
                 (CPU_STK      *)&app_delay_by_real_time_task_stk[0],
                 (CPU_STK_SIZE  )APP_CFG_TASK_COMMON_STK_SIZE / 10,
                 (CPU_STK_SIZE  )APP_CFG_TASK_COMMON_STK_SIZE,
                 (OS_MSG_QTY    )0,
                 (OS_TICK       )0,
                 (void         *)0,
                 (OS_OPT        )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                 (OS_ERR       *)&err);
				 
    OSTaskCreate((OS_TCB       *)&app_delay_resume_task_tcb,            
                 (CPU_CHAR     *)"app delay resume task",
                 (OS_TASK_PTR   )app_delay_resume_task, 
                 (void         *)0,
                 (OS_PRIO       )APP_CFG_TASK_COMMON_PRIO + 2,
                 (CPU_STK      *)&app_delay_resume_task_stk[0],
                 (CPU_STK_SIZE  )APP_CFG_TASK_COMMON_STK_SIZE / 10,
                 (CPU_STK_SIZE  )APP_CFG_TASK_COMMON_STK_SIZE,
                 (OS_MSG_QTY    )0,
                 (OS_TICK       )0,
                 (void         *)0,
                 (OS_OPT        )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                 (OS_ERR       *)&err);
}
