 /**
 * @brief       : 定时器
 *                现象: 每隔一秒打印一次数据,打印三次后停止定时器
 *
 * @file        : app_case_timer.c
 * @author      : cuihongpeng
 * @version     : v0.0.1
 * @date        : 2015/10/12
 *
 * Change Logs  : 
 *
 * Date        Version      Author      Notes
 * 2015/10/12  v0.0.1    cuihongpeng    first version
 */
#include <includes.h>

static OS_TCB  app_start_task_tcb;
static CPU_STK app_start_task_stk[APP_CFG_TASK_START_STK_SIZE];

static void app_task_create(void);
static void app_event_create(void);
static void app_start_task(void *p_arg);
static void app_timer_create(void);

OS_SEM sem_isr;
OS_TMR pTimer0;
OS_SEM sem_pend;

int main(void)
{
    OS_ERR  err;  
    
    /* 初始化uC/OS-III 内核 */
    OSInit(&err);  
    
    OSTaskCreate((OS_TCB       *)&app_start_task_tcb,               /* 任务控制块地址 */           
                 (CPU_CHAR     *)"App Task Start",                  /* 任务名*/
                 (OS_TASK_PTR   )app_start_task,                    /* 启动任务函数地址 */
                 (void         *)0,                                 /* 传递给任务的参数*/
                 (OS_PRIO       )APP_CFG_TASK_START_PRIO,           /* 任务优先级*/
                 (CPU_STK      *)&app_start_task_stk[0],            /* 堆栈基地址 */
                 (CPU_STK_SIZE  )APP_CFG_TASK_START_STK_SIZE / 10,  /* 堆栈监测区*/
                 (CPU_STK_SIZE  )APP_CFG_TASK_START_STK_SIZE,       /* 堆栈空间大小 */
                 (OS_MSG_QTY    )0,                                 /* 本任务支持接受的最大消息数 */
                 (OS_TICK       )0,                                 /* 设置时间*/
                 (void         *)0,                                 /* 堆栈空间大小 */  
                 (OS_OPT        )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                 (OS_ERR       *)&err);

    OSStart(&err);                                               
    
    (void)&err;
    
    return (0);
}

static void app_start_task(void *p_arg)
{
    OS_ERR      err;
    
    (void)p_arg;
    
    CPU_Init();
    bsp_init();
    bsp_tick_init();
    app_event_create();
    app_task_create();  
    app_timer_create();
    
    while (1)
    {  
//		printf("start task case!\r\n\r\n");
		OSTaskDel(&app_start_task_tcb, &err);
    }
}

/**
 * @brief 定时器回调函数
 */
void app_timer_cb(void *ptmr, void *parg)
{
    OS_ERR err;
    static uint32_t run_count =  0;
    printf("Timer :%d\r\n", run_count + 1);
    
    if(run_count++ >= RUN_TIMERS - 1)
    {
        OSTmrStop(&pTimer0, OS_OPT_TMR_NONE, NULL, &err);
        if(err == OS_ERR_NONE)
        {
            printf("stop timer succsee!\r\n");
        }
    }
}

/**
 * @brief 创建信号量
 */
static void app_event_create(void)
{
    OS_ERR       err;
    OSSemCreate(&sem_isr, "sem_isr", 0, &err);
    OSSemCreate(&sem_pend, "sem_pend", 0, &err);
}

/**
 * @brief 创建任务
 */
static void app_task_create(void)
{
                
}

/**
 * @brief 创建定时器
 */
static void app_timer_create(void)
{
    OS_ERR err; 
    
    OSTmrCreate(&pTimer0, "OSTIMER0", 0, 10, OS_OPT_TMR_PERIODIC, app_timer_cb, NULL, &err);
    OSTmrStart(&pTimer0, &err);
    if(err == OS_ERR_NONE)
    {
        printf("start timer succsee!\r\n");
    }
}
