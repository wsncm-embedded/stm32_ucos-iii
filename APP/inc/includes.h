#ifndef  __INCLUDES_H__
#define  __INCLUDES_H__

#include  <stdarg.h>
#include  <stdio.h>
#include  <stdlib.h>
#include  <math.h>
#include  <cpu.h>
#include  <lib_def.h>
#include  <lib_ascii.h>
#include  <lib_math.h>
#include  <lib_mem.h>
#include  <lib_str.h>
#include  <app_cfg.h>
#include  <os.h>
#include  <bsp.h>

#define RUN_TIMERS 3
#define DELAY_TIMER 1000

#define RUN_COUNTER(X)        if(X++ >= RUN_TIMERS)\
								{\
									OSSemPend(&sem_pend, 0, OS_OPT_PEND_BLOCKING, NULL, NULL);\
								}

#endif