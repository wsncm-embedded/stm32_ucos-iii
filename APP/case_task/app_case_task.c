 /**
 * @brief       : 任务 测试用例
 *
 * @file        : app_case_task.c
 * @author      : cuihongpeng
 * @version     : v0.0.1
 * @date        : 2015/10/12
 *
 * Change Logs  : 
 *
 * Date        Version      Author      Notes
 * 2015/11/04  v0.0.1       cuihongpeng    first version
 */
#include "includes.h"

OS_SEM  sem_isr;
OS_SEM  sem_pend;
OS_SEM  sem_task;

static OS_TCB  app_start_task_tcb;
static CPU_STK app_start_task_stk[APP_CFG_TASK_START_STK_SIZE];

static OS_TCB  app_pend_sem_task_tcb;
static CPU_STK app_pend_sem_task_stk[APP_CFG_TASK_COMMON_STK_SIZE];

static OS_TCB  app_pend_sem_from_isr_task_tcb;
static CPU_STK app_pend_sem_from_isr_task_stk[APP_CFG_TASK_COMMON_STK_SIZE];

static OS_TCB  app_post_sem_task_tcb;
static CPU_STK app_post_sem_task_stk[APP_CFG_TASK_COMMON_STK_SIZE];

static void app_task_create(void);
static void app_event_create(void);
static void app_start_task(void *p_arg);

int main(void)
{
    OS_ERR  err;  
    
    /* 初始化uC/OS-III 内核 */
    OSInit(&err);  
    
    OSTaskCreate((OS_TCB       *)&app_start_task_tcb,               /* 任务控制块地址 */           
                 (CPU_CHAR     *)"App Task Start",                  /* 任务名*/
                 (OS_TASK_PTR   )app_start_task,                    /* 启动任务函数地址 */
                 (void         *)0,                                 /* 传递给任务的参数*/
                 (OS_PRIO       )APP_CFG_TASK_START_PRIO,           /* 任务优先级*/
                 (CPU_STK      *)&app_start_task_stk[0],            /* 堆栈基地址 */
                 (CPU_STK_SIZE  )APP_CFG_TASK_START_STK_SIZE / 10,  /* 堆栈监测区*/
                 (CPU_STK_SIZE  )APP_CFG_TASK_START_STK_SIZE,       /* 堆栈空间大小 */
                 (OS_MSG_QTY    )0,                                 /* 本任务支持接受的最大消息数 */
                 (OS_TICK       )0,                                 /* 设置时间*/
                 (void         *)0,                                 /* 堆栈空间大小 */  
                 (OS_OPT        )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                 (OS_ERR       *)&err);

    OSStart(&err);                                               
    
    (void)&err;
    
    return (0);
}

static void app_start_task(void *p_arg)
{
    OS_ERR      err;
    
    (void)p_arg;
    
    CPU_Init();
    bsp_init();
    bsp_tick_init();
    app_event_create();
    app_task_create();   
    
    while (1)
    {  
		printf("start task case!\r\n\r\n");
		OSTaskDel(&app_start_task_tcb, &err);
    }
}

/**
 * @brief 从任务中获取信号量
 */
void app_pend_sem_task(void *p_arg)
{
    while (1)                                            
    {
        static uint8_t count = 0;
		RUN_COUNTER(count);
        OS_ERR err;
        OSSemPend(&sem_task, 0, OS_OPT_PEND_BLOCKING, NULL, &err);
        OSSchedLock(&err);
        printf("pending sem from task success!\r\n\r\n");
        OSSchedUnlock(&err);
    }
}

/**
 * @brief 从中断中获取信号量
 */
void app_pend_sem_from_isr_task(void *p_arg)
{
    while (1)                                            
    {
        OS_ERR err;
        OSSemPend(&sem_isr, 0, OS_OPT_PEND_BLOCKING, NULL, &err);
        printf("pending sem from isr success!\r\n\r\n");
		// OSTaskSemPend((OS_TICK )0,                         
					  // (OS_OPT  )OS_OPT_PEND_BLOCKING,
					  // (CPU_TS *)&ts,
					  // (OS_ERR *)&err);
        // printf("A");
    }
}

/**
 * @brief 发送信号量
 */
void app_post_sem_task(void *p_arg)
{
    while (1)                                            
    {
        static uint8_t count = 0;
		RUN_COUNTER(count);
        OS_ERR err;
        OSSchedLock(&err);
        printf("post sem from task!\r\n");
        OSSchedUnlock(&err);
        OSSemPost(&sem_task, OS_OPT_POST_1, &err);
        OSTimeDly(DELAY_TIMER, OS_OPT_TIME_DLY, &err);
    }
}


/**
 * @brief 创建信号量
 */
static void app_event_create(void)
{
    OS_ERR       err;
    OSSemCreate(&sem_task, "sem_task", 0, &err);
    OSSemCreate(&sem_isr, "sem_isr", 0, &err);
    OSSemCreate(&sem_pend, "sem_pend", 0, &err);
}


static void app_task_create(void)
{
    OS_ERR      err;
    
    OSTaskCreate((OS_TCB       *)&app_pend_sem_task_tcb,            
                 (CPU_CHAR     *)"app pend sem task",
                 (OS_TASK_PTR   )app_pend_sem_task, 
                 (void         *)0,
                 (OS_PRIO       )APP_CFG_TASK_COMMON_PRIO,
                 (CPU_STK      *)&app_pend_sem_task_stk[0],
                 (CPU_STK_SIZE  )APP_CFG_TASK_COMMON_STK_SIZE / 10,
                 (CPU_STK_SIZE  )APP_CFG_TASK_COMMON_STK_SIZE,
                 (OS_MSG_QTY    )0,
                 (OS_TICK       )0,
                 (void         *)0,
                 (OS_OPT        )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                 (OS_ERR       *)&err);
				 
    OSTaskCreate((OS_TCB       *)&app_pend_sem_from_isr_task_tcb,            
                 (CPU_CHAR     *)"app pend sem from isr",
                 (OS_TASK_PTR   )app_pend_sem_from_isr_task, 
                 (void         *)0,
                 (OS_PRIO       )APP_CFG_TASK_COMMON_PRIO + 1,
                 (CPU_STK      *)&app_pend_sem_from_isr_task_stk[0],
                 (CPU_STK_SIZE  )APP_CFG_TASK_COMMON_STK_SIZE / 10,
                 (CPU_STK_SIZE  )APP_CFG_TASK_COMMON_STK_SIZE,
                 (OS_MSG_QTY    )0,
                 (OS_TICK       )0,
                 (void         *)0,
                 (OS_OPT        )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                 (OS_ERR       *)&err);
				 
    OSTaskCreate((OS_TCB       *)&app_post_sem_task_tcb,            
                 (CPU_CHAR     *)"app post sem task",
                 (OS_TASK_PTR   )app_post_sem_task, 
                 (void         *)0,
                 (OS_PRIO       )APP_CFG_TASK_COMMON_PRIO + 2,
                 (CPU_STK      *)&app_post_sem_task_stk[0],
                 (CPU_STK_SIZE  )APP_CFG_TASK_COMMON_STK_SIZE / 10,
                 (CPU_STK_SIZE  )APP_CFG_TASK_COMMON_STK_SIZE,
                 (OS_MSG_QTY    )0,
                 (OS_TICK       )0,
                 (void         *)0,
                 (OS_OPT        )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                 (OS_ERR       *)&err);
}
