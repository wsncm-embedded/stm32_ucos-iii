 /**
 * @brief       : 临界区保护
 *                开关中断：
 *                调度器上锁：测试任务周期执行，顺序打印ABC则测试通过
 *                互斥信号量：测试任务周期执行，顺序打印ABC则测试通过
 * @file        : app_case_critical.c
 * @author      : cuihongpeng
 * @version     : v0.0.1
 * @date        : 2015/11/11
 *
 * Change Logs  : 
 *
 * Date        Version      Author      Notes
 * 2015/11/11  v0.0.1    cuihongpeng    first version
 */
#include <includes.h>

static OS_TCB  app_start_task_tcb;
static CPU_STK app_start_task_stk[APP_CFG_TASK_START_STK_SIZE];

static OS_TCB  app_close_isr_task_tcb;
static CPU_STK app_close_isr_task_stk[APP_CFG_TASK_COMMON_STK_SIZE];

static OS_TCB  app_lock_sched_test_task_tcb;
static CPU_STK app_lock_sched_test_task_stk[APP_CFG_TASK_COMMON_STK_SIZE];

static OS_TCB  app_lock_sched_task_tcb;
static CPU_STK app_lock_sched_task_stk[APP_CFG_TASK_COMMON_STK_SIZE];

static OS_TCB  app_mutex_sem_task_tcb;
static CPU_STK app_mutex_sem_task_stk[APP_CFG_TASK_COMMON_STK_SIZE];

static OS_TCB  app_mutex_test_task_tcb;
static CPU_STK app_mutex_test_task_stk[APP_CFG_TASK_COMMON_STK_SIZE];

OS_SEM sem_isr;
OS_SEM sem_pend;
OS_SEM sem_lock_sched;
OS_SEM sem_task;
OS_MUTEX sem_mutex;

static void app_task_create(void);
static void app_event_create(void);
static void app_start_task(void *p_arg);

int main(void)
{
    OS_ERR  err;  
    
    /* 初始化uC/OS-III 内核 */
    OSInit(&err);  
    
    OSTaskCreate((OS_TCB       *)&app_start_task_tcb,               /* 任务控制块地址 */           
                 (CPU_CHAR     *)"App Task Start",                  /* 任务名*/
                 (OS_TASK_PTR   )app_start_task,                    /* 启动任务函数地址 */
                 (void         *)0,                                 /* 传递给任务的参数*/
                 (OS_PRIO       )APP_CFG_TASK_START_PRIO,           /* 任务优先级*/
                 (CPU_STK      *)&app_start_task_stk[0],            /* 堆栈基地址 */
                 (CPU_STK_SIZE  )APP_CFG_TASK_START_STK_SIZE / 10,  /* 堆栈监测区*/
                 (CPU_STK_SIZE  )APP_CFG_TASK_START_STK_SIZE,       /* 堆栈空间大小 */
                 (OS_MSG_QTY    )0,                                 /* 本任务支持接受的最大消息数 */
                 (OS_TICK       )0,                                 /* 设置时间*/
                 (void         *)0,                                 /* 堆栈空间大小 */  
                 (OS_OPT        )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                 (OS_ERR       *)&err);

    OSStart(&err);                                               
    
    (void)&err;
    
    return (0);
}

static void app_start_task(void *p_arg)
{
    OS_ERR      err;
    
    (void)p_arg;
    
    CPU_Init();
    bsp_init();
    bsp_tick_init();
    app_event_create();
    app_task_create();   
    
    while (1)
    {  
		printf("start task case!\r\n\r\n");
		OSTaskDel(&app_start_task_tcb, &err);
    }
}

/**
 * @brief 关中断方式保护资源
 */
void app_close_isr_task(void *p_arg)
{
    while (1)                                            
    {
		static uint32_t run_count =  0;
        RUN_COUNTER(run_count);
		
        OS_ERR err;
        CPU_SR  cpu_sr;
        OS_CRITICAL_ENTER();// 关中断
        printf("ISR A\r\n");
        OS_CRITICAL_EXIT();// 开中断
        OSTimeDly(DELAY_TIMER, OS_OPT_TIME_DLY, &err);
    }
}

/**
 * @brief 调度器上锁方式测试
 */
void app_lock_sched_test_task(void *p_arg)
{
    while (1)                                            
    {
		static uint32_t run_count =  0;
        RUN_COUNTER(run_count);
		
        OS_ERR err;
		OSSemPend(&sem_lock_sched, 0, OS_OPT_PEND_BLOCKING, NULL, &err);
        printf("lock sched C\r\n");
    }
}

/**
 * @brief 调度器上锁方式保护资源
 */
void app_lock_sched_task(void *p_arg)
{
    while (1)                                            
    {
        static uint32_t run_count =  0;
        RUN_COUNTER(run_count);
		
		OS_ERR err;
        OSSchedLock(&err);
        printf("lock sched A\r\n");
		OSSemPost(&sem_lock_sched, OS_OPT_POST_1, &err);
        printf("lock sched B\r\n");
        OSSchedUnlock(&err);

        OSTimeDly(DELAY_TIMER, OS_OPT_TIME_DLY, &err);
    }
}

/**
 * @brief 互斥信号量方式测试（优先级高于被测试任务）
 */
void app_mutex_test_task(void *p_arg)
{
    while (1)                                            
    {
        static uint32_t run_count =  0;
        RUN_COUNTER(run_count);
		
        OS_ERR err;
		OSSemPend(&sem_task, 0, OS_OPT_PEND_BLOCKING, NULL, &err);
		OSMutexPend(&sem_mutex, 0, OS_OPT_PEND_BLOCKING, NULL, &err);
        printf("mutex C\r\n");
        OSMutexPost(&sem_mutex, OS_OPT_PEND_BLOCKING, &err);
    }
}

/**
 * @brief 互斥信号量方式保护资源
 */
void app_mutex_sem_task(void *p_arg)
{
    while (1)                                            
    {
        static uint32_t run_count =  0;
        RUN_COUNTER(run_count);
		
        OS_ERR err;
        OSMutexPend(&sem_mutex, 0, OS_OPT_PEND_BLOCKING, NULL, &err);
        printf("mutex A\r\n");
		OSSemPost(&sem_task, OS_OPT_POST_1, &err);
        printf("mutex B\r\n");
        OSMutexPost(&sem_mutex, OS_OPT_PEND_BLOCKING, &err);
        
        OSTimeDly(DELAY_TIMER, OS_OPT_TIME_DLY, &err);
    }
}

/**
 * @brief 创建信号量
 */
static void app_event_create(void)
{
    OS_ERR       err;
    OSSemCreate(&sem_task, "sem_task", 0, &err);
    OSSemCreate(&sem_isr, "sem_isr", 0, &err);
    OSSemCreate(&sem_pend, "sem_pend", 0, &err);
    OSSemCreate(&sem_lock_sched, "sem_lock_sched", 0, &err);
	OSMutexCreate(&sem_mutex, "sem_mutex", &err);
}

/**
 * @brief 创建任务
 */
static void app_task_create(void)
{ 
    OS_ERR err;
    
    OSTaskCreate((OS_TCB       *)&app_close_isr_task_tcb,            
                 (CPU_CHAR     *)"app_close_isr_task",
                 (OS_TASK_PTR   )app_close_isr_task, 
                 (void         *)0,
                 (OS_PRIO       )APP_CFG_TASK_COMMON_PRIO,
                 (CPU_STK      *)&app_close_isr_task_stk[0],
                 (CPU_STK_SIZE  )APP_CFG_TASK_COMMON_STK_SIZE / 10,
                 (CPU_STK_SIZE  )APP_CFG_TASK_COMMON_STK_SIZE,
                 (OS_MSG_QTY    )0,
                 (OS_TICK       )0,
                 (void         *)0,
                 (OS_OPT        )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                 (OS_ERR       *)&err);
				 
    OSTaskCreate((OS_TCB       *)&app_lock_sched_test_task_tcb,            
                 (CPU_CHAR     *)"app_lock_sched_test_task",
                 (OS_TASK_PTR   )app_lock_sched_test_task, 
                 (void         *)0,
                 (OS_PRIO       )APP_CFG_TASK_COMMON_PRIO + 1,
                 (CPU_STK      *)&app_lock_sched_test_task_stk[0],
                 (CPU_STK_SIZE  )APP_CFG_TASK_COMMON_STK_SIZE / 10,
                 (CPU_STK_SIZE  )APP_CFG_TASK_COMMON_STK_SIZE,
                 (OS_MSG_QTY    )0,
                 (OS_TICK       )0,
                 (void         *)0,
                 (OS_OPT        )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                 (OS_ERR       *)&err);
				 
    OSTaskCreate((OS_TCB       *)&app_mutex_test_task_tcb,            
                 (CPU_CHAR     *)"app_mutex_test_task",
                 (OS_TASK_PTR   )app_mutex_test_task, 
                 (void         *)0,
                 (OS_PRIO       )APP_CFG_TASK_COMMON_PRIO + 2,
                 (CPU_STK      *)&app_mutex_test_task_stk[0],
                 (CPU_STK_SIZE  )APP_CFG_TASK_COMMON_STK_SIZE / 10,
                 (CPU_STK_SIZE  )APP_CFG_TASK_COMMON_STK_SIZE,
                 (OS_MSG_QTY    )0,
                 (OS_TICK       )0,
                 (void         *)0,
                 (OS_OPT        )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                 (OS_ERR       *)&err);
				 
    OSTaskCreate((OS_TCB       *)&app_lock_sched_task_tcb,            
                 (CPU_CHAR     *)"app_lock_sched_task",
                 (OS_TASK_PTR   )app_lock_sched_task, 
                 (void         *)0,
                 (OS_PRIO       )APP_CFG_TASK_COMMON_PRIO + 3,
                 (CPU_STK      *)&app_lock_sched_task_stk[0],
                 (CPU_STK_SIZE  )APP_CFG_TASK_COMMON_STK_SIZE / 10,
                 (CPU_STK_SIZE  )APP_CFG_TASK_COMMON_STK_SIZE,
                 (OS_MSG_QTY    )0,
                 (OS_TICK       )0,
                 (void         *)0,
                 (OS_OPT        )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                 (OS_ERR       *)&err);
				 
    OSTaskCreate((OS_TCB       *)&app_mutex_sem_task_tcb,            
                 (CPU_CHAR     *)"app_mutex_sem_task",
                 (OS_TASK_PTR   )app_mutex_sem_task, 
                 (void         *)0,
                 (OS_PRIO       )APP_CFG_TASK_COMMON_PRIO + 4,
                 (CPU_STK      *)&app_mutex_sem_task_stk[0],
                 (CPU_STK_SIZE  )APP_CFG_TASK_COMMON_STK_SIZE / 10,
                 (CPU_STK_SIZE  )APP_CFG_TASK_COMMON_STK_SIZE,
                 (OS_MSG_QTY    )0,
                 (OS_TICK       )0,
                 (void         *)0,
                 (OS_OPT        )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                 (OS_ERR       *)&err);
}