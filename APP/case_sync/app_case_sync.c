 /**
 * @brief       : 行为同步
 *                中断任务同步: 串口中发送'1'，会同步任务进行打印
 *                任务单向同步: 单向同步依次打印ABC
 *                任务双向同步: 按优先级依次打印
 *                任务事件同步: 按顺序打印ABC
 * @file        : app_case_sync.c
 * @author      : cuihongpeng
 * @version     : v0.0.1
 * @date        : 2015/10/12
 *
 * Change Logs  : 
 *
 * Date        Version      Author      Notes
 * 2015/10/12  v0.0.1    cuihongpeng    first version
 */
#include <includes.h>

static OS_TCB  app_start_task_tcb;
static CPU_STK app_start_task_stk[APP_CFG_TASK_START_STK_SIZE];

static OS_TCB  app_isr_sync_task_tcb;
static CPU_STK app_isr_sync_task_stk[APP_CFG_TASK_COMMON_STK_SIZE];

static OS_TCB  app_sem_sync_a_task_tcb;
static CPU_STK app_sem_sync_a_task_stk[APP_CFG_TASK_COMMON_STK_SIZE];
static OS_TCB  app_sem_sync_b_task_tcb;
static CPU_STK app_sem_sync_b_task_stk[APP_CFG_TASK_COMMON_STK_SIZE];
static OS_TCB  app_sem_sync_c_task_tcb;
static CPU_STK app_sem_sync_c_task_stk[APP_CFG_TASK_COMMON_STK_SIZE];

static OS_TCB  app_bisynchronous_a_task_tcb;
static CPU_STK app_bisynchronous_a_task_stk[APP_CFG_TASK_COMMON_STK_SIZE];
static OS_TCB  app_bisynchronous_b_task_tcb;
static CPU_STK app_bisynchronous_b_task_stk[APP_CFG_TASK_COMMON_STK_SIZE];

static OS_TCB  app_flag_sync_a_task_tcb;
static CPU_STK app_flag_sync_a_task_stk[APP_CFG_TASK_COMMON_STK_SIZE];
static OS_TCB  app_flag_sync_b_task_tcb;
static CPU_STK app_flag_sync_b_task_stk[APP_CFG_TASK_COMMON_STK_SIZE];
static OS_TCB  app_flag_sync_c_task_tcb;
static CPU_STK app_flag_sync_c_task_stk[APP_CFG_TASK_COMMON_STK_SIZE];


static void app_task_create(void);
static void app_event_create(void);
static void app_start_task(void *p_arg);

OS_SEM sem_isr;
OS_SEM sem_pend;
OS_SEM sem_sync_a;
OS_SEM sem_sync_b;
OS_SEM sem_sync_c;
OS_SEM sem_bisynchronous;
OS_SEM ack_bisynchronous;

OS_FLAG_GRP flagw;

int main(void)
{
    OS_ERR  err;  
    
    /* 初始化uC/OS-III 内核 */
    OSInit(&err);  
    
    OSTaskCreate((OS_TCB       *)&app_start_task_tcb,               /* 任务控制块地址 */           
                 (CPU_CHAR     *)"App Task Start",                  /* 任务名*/
                 (OS_TASK_PTR   )app_start_task,                    /* 启动任务函数地址 */
                 (void         *)0,                                 /* 传递给任务的参数*/
                 (OS_PRIO       )APP_CFG_TASK_START_PRIO,           /* 任务优先级*/
                 (CPU_STK      *)&app_start_task_stk[0],            /* 堆栈基地址 */
                 (CPU_STK_SIZE  )APP_CFG_TASK_START_STK_SIZE / 10,  /* 堆栈监测区*/
                 (CPU_STK_SIZE  )APP_CFG_TASK_START_STK_SIZE,       /* 堆栈空间大小 */
                 (OS_MSG_QTY    )0,                                 /* 本任务支持接受的最大消息数 */
                 (OS_TICK       )0,                                 /* 设置时间*/
                 (void         *)0,                                 /* 堆栈空间大小 */  
                 (OS_OPT        )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                 (OS_ERR       *)&err);

    OSStart(&err);                                               
    
    (void)&err;
    
    return (0);
}

static void app_start_task(void *p_arg)
{
    OS_ERR      err;
    
    (void)p_arg;
    
    CPU_Init();
    bsp_init();
    bsp_tick_init();
    app_event_create();
    app_task_create();   
    
    while (1)
    {  
        printf("start task case!\r\n\r\n");
        OSTaskDel(&app_start_task_tcb, &err);
    }
}

/**
 * @brief ISR同步任务
 */
void app_isr_sync_task(void *p_arg)
{
    while (1)                                            
    {
        OS_ERR err;
        OSSemPend(&sem_isr, 0, OS_OPT_PEND_BLOCKING, NULL, &err);
        OSSchedLock(&err);
        printf("isr sync task\r\n");
        OSSchedUnlock(&err);
    }
}

/**
 * @brief 行为同步任务a
 */
void app_sem_sync_a_task(void *p_arg)
{
    while (1)                                            
    {
        static uint32_t run_count =  0;
        RUN_COUNTER(run_count);
        OS_ERR err;
        
        OSSchedLock(&err);
        printf("sem sync A\r\n");
        OSSchedUnlock(&err);
        
        OSSemPost(&sem_sync_a, OS_OPT_POST_1, &err);
        
        OSTimeDly(DELAY_TIMER, OS_OPT_TIME_DLY, &err);
    }
}

/**
 * @brief 行为同步任务b
 */
void app_sem_sync_b_task(void *p_arg)
{
    while (1)                                            
    {
        static uint32_t run_count =  0;
        RUN_COUNTER(run_count);
        OS_ERR err;
        
        OSSemPend(&sem_sync_a, 0, OS_OPT_PEND_BLOCKING, NULL, &err);
        
        OSSchedLock(&err);
        printf("sem sync B\r\n");
        OSSchedUnlock(&err);
        
        OSSemPost(&sem_sync_b, OS_OPT_POST_1, &err);
    }
}

/**
 * @brief 行为同步任务c
 */
void app_sem_sync_c_task(void *p_arg)
{
    while (1)                                            
    {
        static uint32_t run_count =  0;
        RUN_COUNTER(run_count);
        OS_ERR err;
        
        OSSemPend(&sem_sync_b, 0, OS_OPT_PEND_BLOCKING, NULL, &err);
        
        OSSchedLock(&err);
        printf("sem sync C\r\n\r\n");
        OSSchedUnlock(&err);
       
    }
}

/**
 * @brief 双向同步任务A
 */
void app_bisynchronous_a_task(void *p_arg)
{
    while (1)                                            
    {
        static uint32_t run_count =  0;
        RUN_COUNTER(run_count);
        OS_ERR err;
        
        OSSemPost(&sem_bisynchronous, OS_OPT_POST_1, &err);
        
        OSSchedLock(&err);
        printf("bisynchronous A\r\n");
        OSSchedUnlock(&err);
        
        OSSemPend(&ack_bisynchronous, 0, OS_OPT_PEND_BLOCKING, NULL, &err);
        OSTimeDly(DELAY_TIMER, OS_OPT_TIME_DLY, &err);
    }
}

/**
 * @brief 双向同步任务B
 */
void app_bisynchronous_b_task(void *p_arg)
{
    while (1)                                            
    {
        static uint32_t run_count =  0;
        RUN_COUNTER(run_count);
        OS_ERR err;
        
        OSSemPend(&sem_bisynchronous, 0, OS_OPT_PEND_BLOCKING, NULL, &err);
        
        OSSchedLock(&err);
        printf("bisynchronous B\r\n");
        OSSchedUnlock(&err);
        
        OSSemPost(&ack_bisynchronous, OS_OPT_POST_1, &err);
    }
}

/**
 * @brief 事件同步任务A
 */
void app_flag_sync_a_task(void *p_arg)
{
    while (1)                                            
    {
        static uint32_t run_count =  0;
        RUN_COUNTER(run_count);
        OS_ERR err;
        
        OSSchedLock(&err);
        printf("\r\nflag sync A\r\n");
        OSSchedUnlock(&err);
        
        OSFlagPost(&flagw,
                    0x01, 
                    OS_OPT_POST_FLAG_SET,
                    &err);

        OSTimeDly(DELAY_TIMER, OS_OPT_TIME_DLY, &err);
    }
}

/**
 * @brief 事件同步任务B
 */
void app_flag_sync_b_task(void *p_arg)
{
    while (1)                                            
    {
        static uint32_t run_count =  0;
        RUN_COUNTER(run_count);
        OS_ERR err;
        
        OSSchedLock(&err);
        printf("flag sync B\r\n");
        OSSchedUnlock(&err);
        
        OSFlagPost(&flagw,
                    0x02, 
                    OS_OPT_POST_FLAG_SET,
                    &err);

        OSTimeDly(DELAY_TIMER, OS_OPT_TIME_DLY, &err);
    }
}

/**
 * @brief 事件同步任务C
 */
void app_flag_sync_c_task(void *p_arg)
{
    while (1)                                            
    {
        static uint32_t run_count =  0;
        RUN_COUNTER(run_count);
        OS_ERR err;
        
        OSFlagPend(&flagw,
                    0x03, 
                    0,
                    OS_OPT_PEND_FLAG_SET_ALL
                    + OS_OPT_PEND_FLAG_CONSUME,
                    (CPU_TS *)0,
                    &err);
        
        OSSchedLock(&err);
        printf("flag sync C\r\n\r\n");
        OSSchedUnlock(&err);

    }
}

/**
 * @brief 创建信号量
 */
static void app_event_create(void)
{
    OS_ERR err;
    OSSemCreate(&sem_isr, "sem_isr", 0, &err);
    OSSemCreate(&sem_pend, "sem_pend", 0, &err);
    
    OSSemCreate(&sem_sync_a, "sem_sync_a", 0, &err);
    OSSemCreate(&sem_sync_b, "sem_sync_b", 0, &err);
    OSSemCreate(&sem_sync_c, "sem_sync_c", 0, &err);
    OSSemCreate(&sem_bisynchronous, "sem_bisynchronous", 0, &err);
    OSSemCreate(&ack_bisynchronous, "ack_bisynchronous", 0, &err);

    OSFlagCreate (&flagw, "flagw", 0, &err);
}

/**
 * @brief 创建任务
 */
static void app_task_create(void)
{ 
    OS_ERR err;
    
    OSTaskCreate((OS_TCB       *)&app_sem_sync_a_task_tcb,            
                 (CPU_CHAR     *)"app_sem_sync_a_task",
                 (OS_TASK_PTR   )app_sem_sync_a_task, 
                 (void         *)0,
                 (OS_PRIO       )APP_CFG_TASK_COMMON_PRIO,
                 (CPU_STK      *)&app_sem_sync_a_task_stk[0],
                 (CPU_STK_SIZE  )APP_CFG_TASK_COMMON_STK_SIZE / 10,
                 (CPU_STK_SIZE  )APP_CFG_TASK_COMMON_STK_SIZE,
                 (OS_MSG_QTY    )0,
                 (OS_TICK       )0,
                 (void         *)0,
                 (OS_OPT        )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                 (OS_ERR       *)&err);

    OSTaskCreate((OS_TCB       *)&app_sem_sync_b_task_tcb,            
                 (CPU_CHAR     *)"app_sem_sync_b_task",
                 (OS_TASK_PTR   )app_sem_sync_b_task, 
                 (void         *)0,
                 (OS_PRIO       )APP_CFG_TASK_COMMON_PRIO + 1,
                 (CPU_STK      *)&app_sem_sync_b_task_stk[0],
                 (CPU_STK_SIZE  )APP_CFG_TASK_COMMON_STK_SIZE / 10,
                 (CPU_STK_SIZE  )APP_CFG_TASK_COMMON_STK_SIZE,
                 (OS_MSG_QTY    )0,
                 (OS_TICK       )0,
                 (void         *)0,
                 (OS_OPT        )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                 (OS_ERR       *)&err);              

    OSTaskCreate((OS_TCB       *)&app_sem_sync_c_task_tcb,            
                 (CPU_CHAR     *)"app_sem_sync_c_task",
                 (OS_TASK_PTR   )app_sem_sync_c_task, 
                 (void         *)0,
                 (OS_PRIO       )APP_CFG_TASK_COMMON_PRIO + 2,
                 (CPU_STK      *)&app_sem_sync_c_task_stk[0],
                 (CPU_STK_SIZE  )APP_CFG_TASK_COMMON_STK_SIZE / 10,
                 (CPU_STK_SIZE  )APP_CFG_TASK_COMMON_STK_SIZE,
                 (OS_MSG_QTY    )0,
                 (OS_TICK       )0,
                 (void         *)0,
                 (OS_OPT        )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                 (OS_ERR       *)&err);

    OSTaskCreate((OS_TCB       *)&app_bisynchronous_a_task_tcb,            
                 (CPU_CHAR     *)"app_bisynchronous_a_task",
                 (OS_TASK_PTR   )app_bisynchronous_a_task, 
                 (void         *)0,
                 (OS_PRIO       )APP_CFG_TASK_COMMON_PRIO + 3,
                 (CPU_STK      *)&app_bisynchronous_a_task_stk[0],
                 (CPU_STK_SIZE  )APP_CFG_TASK_COMMON_STK_SIZE / 10,
                 (CPU_STK_SIZE  )APP_CFG_TASK_COMMON_STK_SIZE,
                 (OS_MSG_QTY    )0,
                 (OS_TICK       )0,
                 (void         *)0,
                 (OS_OPT        )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                 (OS_ERR       *)&err);

    OSTaskCreate((OS_TCB       *)&app_bisynchronous_b_task_tcb,            
                 (CPU_CHAR     *)"app_bisynchronous_b_task",
                 (OS_TASK_PTR   )app_bisynchronous_b_task, 
                 (void         *)0,
                 (OS_PRIO       )APP_CFG_TASK_COMMON_PRIO + 4,
                 (CPU_STK      *)&app_bisynchronous_b_task_stk[0],
                 (CPU_STK_SIZE  )APP_CFG_TASK_COMMON_STK_SIZE / 10,
                 (CPU_STK_SIZE  )APP_CFG_TASK_COMMON_STK_SIZE,
                 (OS_MSG_QTY    )0,
                 (OS_TICK       )0,
                 (void         *)0,
                 (OS_OPT        )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                 (OS_ERR       *)&err);

    OSTaskCreate((OS_TCB       *)&app_flag_sync_a_task_tcb,            
                 (CPU_CHAR     *)"app_flag_sync_a_task",
                 (OS_TASK_PTR   )app_flag_sync_a_task, 
                 (void         *)0,
                 (OS_PRIO       )APP_CFG_TASK_COMMON_PRIO + 5,
                 (CPU_STK      *)&app_flag_sync_a_task_stk[0],
                 (CPU_STK_SIZE  )APP_CFG_TASK_COMMON_STK_SIZE / 10,
                 (CPU_STK_SIZE  )APP_CFG_TASK_COMMON_STK_SIZE,
                 (OS_MSG_QTY    )0,
                 (OS_TICK       )0,
                 (void         *)0,
                 (OS_OPT        )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                 (OS_ERR       *)&err);

    OSTaskCreate((OS_TCB       *)&app_flag_sync_b_task_tcb,            
                 (CPU_CHAR     *)"app_flag_sync_b_task",
                 (OS_TASK_PTR   )app_flag_sync_b_task, 
                 (void         *)0,
                 (OS_PRIO       )APP_CFG_TASK_COMMON_PRIO + 6,
                 (CPU_STK      *)&app_flag_sync_b_task_stk[0],
                 (CPU_STK_SIZE  )APP_CFG_TASK_COMMON_STK_SIZE / 10,
                 (CPU_STK_SIZE  )APP_CFG_TASK_COMMON_STK_SIZE,
                 (OS_MSG_QTY    )0,
                 (OS_TICK       )0,
                 (void         *)0,
                 (OS_OPT        )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                 (OS_ERR       *)&err);

    OSTaskCreate((OS_TCB       *)&app_flag_sync_c_task_tcb,            
                 (CPU_CHAR     *)"app_flag_sync_c_task",
                 (OS_TASK_PTR   )app_flag_sync_c_task, 
                 (void         *)0,
                 (OS_PRIO       )APP_CFG_TASK_COMMON_PRIO + 7,
                 (CPU_STK      *)&app_flag_sync_c_task_stk[0],
                 (CPU_STK_SIZE  )APP_CFG_TASK_COMMON_STK_SIZE / 10,
                 (CPU_STK_SIZE  )APP_CFG_TASK_COMMON_STK_SIZE,
                 (OS_MSG_QTY    )0,
                 (OS_TICK       )0,
                 (void         *)0,
                 (OS_OPT        )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                 (OS_ERR       *)&err);

    OSTaskCreate((OS_TCB       *)&app_isr_sync_task_tcb,            
                 (CPU_CHAR     *)"app_isr_sync_task",
                 (OS_TASK_PTR   )app_isr_sync_task, 
                 (void         *)0,
                 (OS_PRIO       )APP_CFG_TASK_COMMON_PRIO + 8,
                 (CPU_STK      *)&app_isr_sync_task_stk[0],
                 (CPU_STK_SIZE  )APP_CFG_TASK_COMMON_STK_SIZE / 10,
                 (CPU_STK_SIZE  )APP_CFG_TASK_COMMON_STK_SIZE,
                 (OS_MSG_QTY    )0,
                 (OS_TICK       )0,
                 (void         *)0,
                 (OS_OPT        )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                 (OS_ERR       *)&err);

}
