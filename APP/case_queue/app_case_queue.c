 /**
 * @brief       : 消息队列 测试用例
 * @file        : app_case_queue.c
 * @author      : cuihongpeng
 * @version     : v0.0.1
 * @date        : 2015/11/11
 *
 * Change Logs  : 
 *
 * Date        Version      Author      Notes
 * 2015/11/11  v0.0.1    cuihongpeng    first version
 */
#include <includes.h>

#define QUEUE_MAX_LEN 3
#define DATA_MAX_LEN sizeof(student_info_all[0])

OS_SEM  sem_isr;
OS_SEM  sem_pend;

static OS_TCB  app_start_task_tcb;
static CPU_STK app_start_task_stk[APP_CFG_TASK_START_STK_SIZE];

static OS_TCB  app_queue_producer_task_tcb;
static CPU_STK app_queue_producer_task_stk[APP_CFG_TASK_COMMON_STK_SIZE];

static OS_TCB  app_queue_consumer_task_tcb;
static CPU_STK app_queue_consumer_task_stk[APP_CFG_TASK_COMMON_STK_SIZE];

static OS_TCB  app_start_task_tcb;
static CPU_STK app_start_task_stk[APP_CFG_TASK_COMMON_STK_SIZE];

typedef struct student_info_t_
{
    char name[20];
    char age[4];
} student_info_t;

student_info_t student_info_all[3] = 
{
    "Hanmeimei","23\0",
    "Lilei","24\0",
    "Lucy","25\0"
};

OS_Q msg_queue;

OS_MEM partition;
uint8_t partition_storage[QUEUE_MAX_LEN][DATA_MAX_LEN];

static void app_task_create(void);
static void app_event_create(void);
static void app_start_task(void *p_arg);

int main(void)
{
    OS_ERR  err;  
    
    /* 初始化uC/OS-III 内核 */
    OSInit(&err);  
    
    OSTaskCreate((OS_TCB       *)&app_start_task_tcb,               /* 任务控制块地址 */           
                 (CPU_CHAR     *)"App Task Start",                  /* 任务名*/
                 (OS_TASK_PTR   )app_start_task,                    /* 启动任务函数地址 */
                 (void         *)0,                                 /* 传递给任务的参数*/
                 (OS_PRIO       )APP_CFG_TASK_START_PRIO,           /* 任务优先级*/
                 (CPU_STK      *)&app_start_task_stk[0],            /* 堆栈基地址 */
                 (CPU_STK_SIZE  )APP_CFG_TASK_START_STK_SIZE / 10,  /* 堆栈监测区*/
                 (CPU_STK_SIZE  )APP_CFG_TASK_START_STK_SIZE,       /* 堆栈空间大小 */
                 (OS_MSG_QTY    )0,                                 /* 本任务支持接受的最大消息数 */
                 (OS_TICK       )0,                                 /* 设置时间*/
                 (void         *)0,                                 /* 堆栈空间大小 */  
                 (OS_OPT        )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                 (OS_ERR       *)&err);

    OSStart(&err);                                               
    
    (void)&err;
    
    return (0);
}

static void app_start_task(void *p_arg)
{
    OS_ERR      err;
    
    (void)p_arg;
    
    CPU_Init();
    bsp_init();
    bsp_tick_init();
    app_event_create();
    app_task_create();   
    
    while (1)
    {  
        printf("start task case!\r\n\r\n");
        OSTaskDel(&app_start_task_tcb, &err);
    }
}

/**
 * @brief 生产者任务(消息队列)
 */
void app_queue_producer_task(void *p_arg)
{
    while (1)                                            
    {
        static uint8_t run_count =  0;
        RUN_COUNTER(run_count);
        
        OS_ERR err;
        student_info_t *pt, *pt_temp, student_info_temp = student_info_all[run_count - 1];
        
        OSMemGet(&partition, &err);
        pt = (student_info_t *)&partition;
        pt_temp = pt;
        memcpy(pt, &student_info_temp, DATA_MAX_LEN);
        
        OSSchedLock(&err);
        printf("queue producer pend student info:\r\nname: %s\r\nage: %s\r\n",pt->name,pt->age);
        OSSchedUnlock(&err);
        
        OSQPost(&msg_queue, (void *)pt_temp, sizeof(student_info_t), OS_OPT_POST_FIFO, &err);

        OSTimeDly(DELAY_TIMER, OS_OPT_TIME_DLY, &err);
    }
}

/**
 * @brief 消费者任务(消息队列)
 */
void app_queue_consumer_task(void *p_arg)
{
    while (1)                                            
    {
        static uint32_t run_count =  0;
        RUN_COUNTER(run_count);
        
        OS_ERR err;
        OS_MSG_SIZE size;
        student_info_t *pt, *pt0;
        
        pt = OSQPend(&msg_queue, 0, OS_OPT_PEND_BLOCKING, &size, NULL, &err);
        pt0 = pt;
        
        OSSchedLock(&err);
        printf("queue producer post student info:\r\nname: %s\r\nage: %s\r\n\r\n", pt->name, pt->age);
        OSSchedUnlock(&err);
        
        OSMemPut(&partition, pt0, &err);
    }
}

/**
 * @brief 创建信号量
 */
static void app_event_create(void)
{
    OS_ERR err;
    
    OSSemCreate(&sem_pend, "sem_pend", 0, &err);
    
    OSQCreate(&msg_queue, "msg_queue", QUEUE_MAX_LEN, &err);
    OSMemCreate(&partition, "partition", &partition_storage[0][0], QUEUE_MAX_LEN, DATA_MAX_LEN, &err);
}

/**
 * @brief 创建任务
 */
static void app_task_create(void)
{ 
    OS_ERR      err;
    
    OSTaskCreate((OS_TCB       *)&app_queue_producer_task_tcb,            
                 (CPU_CHAR     *)"app_queue_producer_task",
                 (OS_TASK_PTR   )app_queue_producer_task, 
                 (void         *)0,
                 (OS_PRIO       )APP_CFG_TASK_COMMON_PRIO,
                 (CPU_STK      *)&app_queue_producer_task_stk[0],
                 (CPU_STK_SIZE  )APP_CFG_TASK_COMMON_STK_SIZE / 10,
                 (CPU_STK_SIZE  )APP_CFG_TASK_COMMON_STK_SIZE,
                 (OS_MSG_QTY    )0,
                 (OS_TICK       )0,
                 (void         *)0,
                 (OS_OPT        )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                 (OS_ERR       *)&err);

    OSTaskCreate((OS_TCB       *)&app_queue_consumer_task_tcb,            
                 (CPU_CHAR     *)"app_queue_consumer_task",
                 (OS_TASK_PTR   )app_queue_consumer_task, 
                 (void         *)0,
                 (OS_PRIO       )APP_CFG_TASK_COMMON_PRIO + 1,
                 (CPU_STK      *)&app_queue_consumer_task_stk[0],
                 (CPU_STK_SIZE  )APP_CFG_TASK_COMMON_STK_SIZE / 10,
                 (CPU_STK_SIZE  )APP_CFG_TASK_COMMON_STK_SIZE,
                 (OS_MSG_QTY    )0,
                 (OS_TICK       )0,
                 (void         *)0,
                 (OS_OPT        )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                 (OS_ERR       *)&err);
}
